#include "coup.hpp"

#include <iostream>

std::ostream& operator<<(std::ostream& s, const Direction& d) {
  switch (d) {
  case Direction::haut   : return s << "haut";
  case Direction::bas    : return s << "bas";
  case Direction::gauche : return s << "gauche";
  case Direction::droite : return s << "droite";
  }
  return s;
}

std::ostream& operator<<(std::ostream& s, const Coup& c) {
  return s << '[' << c.ligne << ',' << c.colonne << "] " << c.direction;
}

