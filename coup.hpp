#ifndef SOLITAIRE_COUP_HPP
#define SOLITAIRE_COUP_HPP

#include <iostream>

// Les directions possible pour un coup.
enum class Direction {haut, bas, gauche, droite};

std::ostream& operator<<(std::ostream& s, const Direction& d);

/* Un coup du jeu.
 * Un coup est définit par un couple de coordonnée et une direction.
 */
struct Coup {
  int ligne;
  int colonne;
  Direction direction;

  Coup(int l, int c, Direction d) : ligne(l), colonne(c), direction(d) {}
}; // Coup

std::ostream& operator<<(std::ostream& s, const Coup& c);

#endif // SOLITAIRE_COUP_HPP
