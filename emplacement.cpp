#include "emplacement.hpp"

Emplacement::Emplacement(): m_etat(Etat::indisponible) { }

bool Emplacement::estPlein() const {
  return m_etat == Etat::plein;
}

bool Emplacement::estVide() const {
  return m_etat == Etat::vide;
}

void Emplacement::devient(Etat e) {
  m_etat = e;
}

void Emplacement::enleve() {
  m_etat = Etat::vide;
}
  
void Emplacement::affiche(std::ostream& s) const {
  switch (m_etat) {
  case Etat::indisponible :
    s << ' ';
    break;
  case Etat::vide :
    s << 'o';
    break;
  case Etat::plein :
    s << 'x';
    break;
  }
}

std::ostream& operator<<(std::ostream& s, const Emplacement& e) {
  e.affiche(s);
  return s;
}
