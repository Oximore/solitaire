#ifndef SOLITAIRE_EMPLACEMENT_HPP
#define SOLITAIRE_EMPLACEMENT_HPP

#include <iostream>

// États possible pour un emplacement.
enum class Etat { indisponible, vide, plein };

// Un trou.
class Emplacement {
public:
  Emplacement();

  bool estPlein() const;
  bool estVide() const;
  
  void devient(Etat e);
  void enleve();

  void affiche(std::ostream& s) const;

private:
  Etat m_etat;
}; // Emplacement

std::ostream& operator<<(std::ostream& s, const Emplacement& e);

#endif // SOLITAIRE_EMPLACEMENT_HPP
