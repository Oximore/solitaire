#include "partie.hpp"


#include "coup.hpp"
#include "plateau.hpp"

Partie::Partie() {}

void Partie::lance() {
  using std::endl;
  std::ostream& s = std::cout;
  Plateau p;
  p.affiche(s);
  s << endl;
  
  //*
  const int hauteur = p.m_plateau.size();
  const int largeur = p.m_plateau.at(0).size();

  bool solution_trouvee = false;
  bool jouer_un_coup = true;
  
  int nombre_de_partie = 0;
  int score = hauteur*largeur;
  int score_max = 0;

  bool premiere_fois = true;

  do {
    int ligne = 0;
    int colonne = 0;
    int direction = 0;
    //Direction direction = Direction::haut;
    
    bool ignore_le_premier = false;

    // Si on n'a pas réussit à jouer un coup au tour prédent,
    // alors on annule le coup précédent et on repart de là dans le test des coups.
    // Si la liste des coup est vide, alors c'est le premier tour
    if ( !jouer_un_coup && !premiere_fois ) {
      if (m_liste_des_coups.empty()) {
	// On est revenu au point de départ,
	// donc le tout premier coup n'était pas bon.
	// Hors le plateau est symétrique,
	// Donc il n'y a pas de solution (?)
	s << "Aucune solution n'a été trouvée." << endl
	  << "Vérifiez la taille du plateau." << endl;
	return;
      }
      
      // On annule le dernier coup
      Coup c = m_liste_des_coups.back();
      m_liste_des_coups.pop_back();
      if ( !p.annuler(c) ) {
	std::cerr << "Une erreur est survenue."<< endl
		  << "Le coup " << c << " n'a pas pu être annulé."<< endl
		  << p << endl;
	return;
      }

      ligne     = c.ligne;
      colonne   = c.colonne;
      direction = static_cast<int>(c.direction);
      
      ignore_le_premier = true;
    }
    premiere_fois = false;
    jouer_un_coup = false;
    
    // ligne 
    if ( !ignore_le_premier ) { ligne = 0; }
    for ( ; ligne < hauteur && !jouer_un_coup ; ++ligne) {
      // colonne
      if ( !ignore_le_premier ) { colonne = 0; }
      for ( ; colonne < largeur && !jouer_un_coup ; ++colonne) {
	
	// direction
	if ( !ignore_le_premier ) { direction = 0; }
	for ( ; direction < 4 && !jouer_un_coup ; ++direction ) {
	  
	  if ( ignore_le_premier ) {
	    ignore_le_premier = false;
	  }
	  else {
	    Coup c(ligne, colonne, static_cast<Direction>(direction));
	    
	    // Si viable le jouer et l'ajouter à la liste des coup jouer
	    if ( p.joue(c) ) {
	      m_liste_des_coups.push_back(c);
	      jouer_un_coup = true;
	    }
	  }
    
	} // direction
      } // colonne 
    } // ligne
    
    // Si il ne reste plus qu'un pion on a une solution
    if ( p.nombrePlein() == 1 ) {
      solution_trouvee = true;
    }
    // Sinon si on n'a pas réussi à jouer de coup, aucun n'est possible
    else if ( !jouer_un_coup ) {
      // le dernier coup n'était pas le bon
      int nombre_pionts = p.nombrePlein();
      score = (score > nombre_pionts) ? nombre_pionts : score;
      score_max = (score_max < nombre_pionts) ? nombre_pionts : score_max;

      ++nombre_de_partie;
      if ( !(nombre_de_partie%10000) ) {
	s << nombre_de_partie << endl;
      }
    }
    else if ( !m_liste_des_coups.empty() ) {
    }

    // On continue tant qu'on a pas trouver de solution
  } while (!solution_trouvee);

  // On a trouvé une solution, on l'affiche
  s << "Une liste de coup gagnant est :" << endl;
  afficheCoups(s);

  s << endl
    << "Un total de " << nombre_de_partie << " parties ont été joué."
    << "Le pire score est de " << score_max << endl;

  Plateau p2;
  for ( const Coup& c : m_liste_des_coups ) {   
    p2.joue(c);
    s << c << endl
      << p2 << endl;
  } 


  /*

  Coup ch(5, 3, Direction::haut);
  Coup cb(1, 3, Direction::bas);
  Coup cg(3, 5, Direction::gauche);
  Coup cd(3, 1, Direction::droite);
  
  Coup& c = cg;

  m_liste_des_coups.push_back(c);
  p.joue(c);

  s << "Premier coup :" << endl;
  afficheCoups(s);
  s << p << endl;


  s << "Coup annulé :" << endl;
  p.annuler(c);
  s << p << endl;
  // */
}
  
void Partie::afficheCoups(std::ostream& s) const {
  for ( const Coup& c : m_liste_des_coups ) {
    s << c << std::endl;
  } 
}

void Partie::affichePlateau(std::ostream& s) const {}
void Partie::affiche(std::ostream& s) const {}

std::ostream& operator<<(std::ostream& s, const Partie& p) {
  p.affiche(s);
  return s;
}
