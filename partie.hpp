#ifndef SOLITAIRE_PARTIE_HPP
#define SOLITAIRE_PARTIE_HPP

#include <iostream>
#include <list>

#include "coup.hpp"

/* Une partie du jeu de solitaire.
 *
 * Plus précisément c'est ici qu'est implémenté l'agorythme pour trouver au moins une solution au jeu.
 */
class Partie {
public:
  Partie();

  void lance();  
  void affiche(std::ostream& s) const;
  void afficheCoups(std::ostream& s) const;
  void affichePlateau(std::ostream& s) const;

private:
  std::list<Coup> m_liste_des_coups;
}; // Partie

std::ostream& operator<<(std::ostream& s, const Partie& p);

#endif // SOLITAIRE_PARTIE_HPP
