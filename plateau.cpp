#include "plateau.hpp"

#include <stdexcept>

// TODO : Trouver une façon de ne pas le redéfinir ici
// Une définition interne pour un coup.
using EmplacementCoup = std::tuple<Emplacement&, Emplacement&, Emplacement&>;
using EmplacementCoup_const = std::tuple<const Emplacement&, const Emplacement&, const Emplacement&>;

Plateau::Plateau() {
  // TODO Vérifier les tailles de matrices ici.
  fixeTaille(7, 7);
  reinitialise(1, 5);
  emplacement(3, 3).enleve();
}

int Plateau::nombrePlein() const {
  int nombre_plein = 0;
  for (const auto ligne : m_plateau) {
    for (const auto emplacement : ligne) {
      if (emplacement.estPlein()) {
	nombre_plein += 1;
      }
    }
  }
  return nombre_plein;
}
  
void Plateau::affiche(std::ostream& s) const {
  for (const auto ligne : m_plateau) {
    for (const auto emplacement : ligne) {
      s << emplacement << ' ';
    }
    s << std::endl;
  }
}

bool Plateau::joue(const Coup& c) {
  if ( !estValide(c) ) {
    return false;
  }

  // Ici le coup est valide, donc obtienEmplacements ne doit pas throw
  const EmplacementCoup ec = obtientEmplacements(c);
  Emplacement& origine     = std::get<0>(ec); 
  Emplacement& milieu      = std::get<1>(ec); 
  Emplacement& destination = std::get<2>(ec); 
  
  // Joue le coup
  origine.devient(Etat::vide);
  milieu.devient(Etat::vide);
  destination.devient(Etat::plein);
  return true;
}
  
bool Plateau::estValide(const Coup& c) const {
  try {
    const EmplacementCoup_const ec = obtientEmplacements(c);
    const Emplacement& origine     = std::get<0>(ec); 
    const Emplacement& milieu      = std::get<1>(ec); 
    const Emplacement& destination = std::get<2>(ec); 
      
    // Définition d'un coup valide
    if ( origine.estPlein() && milieu.estPlein() && destination.estVide() ) {
      return true;
    }
    return false;

  } catch (const std::out_of_range& oor) {
    // Si un des emplacements n'est pas sur le plateau
    return false;
  }
}

bool Plateau::annuler(const Coup& c) {
  try {
    const EmplacementCoup ec = obtientEmplacements(c);
    Emplacement& origine     = std::get<0>(ec); 
    Emplacement& milieu      = std::get<1>(ec); 
    Emplacement& destination = std::get<2>(ec); 
    
    // Définition d'un coup qui vient d'être jouer
    if ( origine.estVide() && milieu.estVide() && destination.estPlein() ) {
      // Annule le coup
      origine.devient(Etat::plein);
      milieu.devient(Etat::plein);
      destination.devient(Etat::vide);
      return true;
    }
    return false;

  } catch (const std::out_of_range& oor) {
    // Si un des emplacements n'est pas sur le plateau
    return false;
  }
  // return false;
}


EmplacementCoup_const Plateau::obtientEmplacements(const Coup& c) const {
  const auto modificateur = obtientModificateur(c.direction);
  const int ligne_modificateur   = std::get<0>(modificateur);
  const int colonne_modificateur = std::get<1>(modificateur);
    
  const Emplacement& origine     = emplacement(c.ligne, c.colonne);
  const Emplacement& milieu      = emplacement(c.ligne+(1*ligne_modificateur), c.colonne+(1*colonne_modificateur));
  const Emplacement& destination = emplacement(c.ligne+(2*ligne_modificateur), c.colonne+(2*colonne_modificateur));
  return std::forward_as_tuple(origine, milieu, destination);
}

EmplacementCoup Plateau::obtientEmplacements(const Coup& c) {
  const auto modificateur = obtientModificateur(c.direction);
  const int ligne_modificateur   = std::get<0>(modificateur);
  const int colonne_modificateur = std::get<1>(modificateur);
    
  Emplacement& origine     = emplacement(c.ligne, c.colonne);
  Emplacement& milieu      = emplacement(c.ligne+(1*ligne_modificateur), c.colonne+(1*colonne_modificateur));
  Emplacement& destination = emplacement(c.ligne+(2*ligne_modificateur), c.colonne+(2*colonne_modificateur));
  return std::forward_as_tuple(origine, milieu, destination);
}

std::tuple<int, int> Plateau::obtientModificateur(const Direction d) const {
  switch (d) {
  case Direction::haut :
    return std::make_tuple(-1,0);
  case Direction::bas :
    return std::make_tuple(1,0);
  case Direction::gauche :
    return std::make_tuple(0,-1);
  case Direction::droite :
    return std::make_tuple(0,1);
  }
  return std::make_tuple(0,0);
}
  
void Plateau::fixeTaille(int hauteur, int largeur) {
  m_plateau = std::vector<std::vector<Emplacement>>(hauteur, std::vector<Emplacement>(largeur, Emplacement()));
}

Emplacement& Plateau::emplacement(int ligne, int colonne) {
  return m_plateau.at(ligne).at(colonne);
}

const Emplacement& Plateau::emplacement(int ligne, int colonne) const {
  return m_plateau.at(ligne).at(colonne);
}
    
void Plateau::reinitialise(const int min, const int max) {
  for ( auto i_ligne = 0 ; i_ligne < m_plateau.size() ; ++i_ligne ) {
    for ( auto i_colonne = 0 ; i_colonne < m_plateau[i_ligne].size() ; ++i_colonne ) {
      if (    ( min < i_ligne   && i_ligne   < max)
	      || ( min < i_colonne && i_colonne < max) ) {
	emplacement(i_ligne, i_colonne).devient(Etat::plein);
      }
      else {
	emplacement(i_ligne, i_colonne).devient(Etat::indisponible);
      }
    }
  }
}

std::ostream& operator<<(std::ostream& s, const Plateau& p) {
  p.affiche(s);
  return s;
}
