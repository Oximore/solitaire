#ifndef SOLITAIRE_PLATEAU_HPP
#define SOLITAIRE_PLATEAU_HPP

#include <iostream>
#include <vector>
#include <tuple>

#include "emplacement.hpp"
#include "coup.hpp"

class Plateau {
public:
  std::vector<std::vector<Emplacement>> m_plateau;

  Plateau();
  
  bool joue(const Coup& c);
  bool estValide(const Coup& c) const;
  bool annuler(const Coup& c);

  int nombrePlein() const;
  void affiche(std::ostream& s) const;

private:
  // Une définition interne pour un coup.
  using EmplacementCoup = std::tuple<Emplacement&, Emplacement&, Emplacement&>;
  using EmplacementCoup_const = std::tuple<const Emplacement&, const Emplacement&, const Emplacement&>;

  Emplacement& emplacement(int ligne, int colonne);
  const Emplacement& emplacement(int ligne, int colonne) const;
    
  EmplacementCoup_const obtientEmplacements(const Coup& c) const;
  EmplacementCoup obtientEmplacements(const Coup& c);
  std::tuple<int, int> obtientModificateur(const Direction d) const;
  
  void fixeTaille(int hauteur, int largeur);
  void reinitialise(const int min, const int max);
}; // Plateau

std::ostream& operator<<(std::ostream& s, const Plateau& p);

#endif // SOLITAIRE_PLATEAU_HPP
