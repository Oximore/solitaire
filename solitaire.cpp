/* solitaire.cpp
 * 
 * 01/04/18
 * Benjamin Lux
 *
 * Parce que je n'arrive plus à retrouver cette combinaison !
 * Et je commente en fraçais si je veux !
 *
 * g++ -g -std=c++11 *.cpp -o solitaire
 */

#include <iostream>

#include "partie.hpp"

// C'est ici que tout commence.
// Du moins dans l'histoire.
int main () {
  std::cout << "Solitaire." << std::endl;

  Partie p;
  p.lance();
  
  return EXIT_SUCCESS;
} // main
